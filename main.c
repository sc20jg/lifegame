
#include<stdlib.h>
#include<stdio.h>
#include <stdbool.h>
#include <SDL2/SDL.h>

#include "controlFunctions.h"
int **data;
int **datap;
int row,col;
void next(){
	for (int i=0;i<row+2;i++){
		for (int j=0;j<col+2;j++){
			if(i==0||j==0||i==row+2||j==col+1){
				datap[i][j]=2;
			}
			else{
				datap[i][j]=data[i-1][j-1];
			}
		}
	}
	for (int i=1;i<row+1;i++){
		for (int j=1;j<col+1;j++){
			if(check(i,j)==1){
				change(i-1,j-1);
			}
		}
	}
}
void input(){
	FILE *fp;
	fp = fopen("data.txt", "r");
	fscanf(fp, "%d %d",&row,&col);
	data=(int **)malloc(sizeof(int *)*row);
	for (int i=0;i<row;i++){
		data[i]=(int *)malloc(sizeof(int)*col);
	}
	
	datap=(int **)malloc(sizeof(int *)*(row+2));
	for (int i=0;i<row+2;i++){
		data[i]=(int *)malloc(sizeof(int)*(col+2));
	}
	
	for(int i=0;i<row;i++){
		for(int j=0;j<col;j++){
			fscanf(fp,"%d",&data[i][j]);
		}
	}
	fclose(fp);
}
	
int main( void )
{
	input();
}

